const request = require("superagent");
const express = require("express");
const bodyParser = require("body-parser");

// Create trivial express server to test with
const app = express();
app.use(bodyParser.json());
app.patch("/json-merge-patch", (req, res) => res.json({requestBody: req.body}));
app.post("/json-merge-patch", (req, res) => res.json({requestBody: req.body}));

if (require.main === module) {
  app
    .listen(3000, () => {
      const runPost = () => request
            .post("localhost:3000/json-merge-patch")
            .send({data: "some data"}) // sends a JSON post body
            .set("Content-Type", "application/json")
            .set("Accept", "application/json")
            .then(res => res.body);

      const runPatch = () => request
            .patch("localhost:3000/json-merge-patch")
            .send({data: "some data"}) // sends a JSON post body
            .set("Content-Type", "application/merge-patch+json")
            .set("Accept", "application/json")
            .then(res => res.body);

      Promise.all([
        runPost(),
        runPatch(),
      ])
            .then(([postBody, patchBody]) => {
              console.log("[POST] returned JSON?", postBody);
              console.log("[PATCH] returned JSON?", patchBody);

              process.exit(JSON.stringify(postBody) === JSON.stringify(patchBody) ? 0 : 1);
            });
    });
}
