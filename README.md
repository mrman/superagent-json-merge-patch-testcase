# Superagent JSON Merge PATCH testcase #

Testcase showing that JSON merge patch (https://tools.ietf.org/html/rfc7386) support in superagent is broken

https://github.com/visionmedia/superagent/issues/1404

# To run #

0. Clone this repository
1. `npm install`
2. `npm run test`
